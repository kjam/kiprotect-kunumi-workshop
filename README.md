## Running Jupyter Locally via Docker

We've shared the same Dockerfile we use for our JupyterHub session in this repository, to make sure you can run all of these notebooks in your own time, on your own machine. This isn't required during class, but can be useful for learning once the session is over.

You will need to have Docker installed on your system to create images and run containers. You can find the installation steps for all platforms on the company's [website](https://docs.docker.com/install/)
.

1) Clone the repository.

2) Once you have Docker installed, type the following on your terminal to create a Docker image: `docker build -t mlsec .` (note the period)

3) That will take a little while to create a Docker image, but once completed, you can run your server with the following:
`docker run -p 8888:8888 mlsec`

4) Head to `localhost:8888` in your browser and you will be able to access the Jupyter Notebooks.


### Local Installation

**All of the coding exercises in the course will be hosted on JupyterHub, and we'll send the URL out at the start of class. Purely browser-based, no installations required. You will be able to download the jupyter notebooks to save.**

If you would like to install locally, please follow these steps:

These lessons has been tested for Python 3.4 and Python 3.6 and primarily uses the latest release of each library, except where versions are pinned. You likely can run most of the code with older releases, but if you run into an issue, try upgrading the library in question first.

```pip install -r requirements.txt```


I believe this will also work with Conda, although I am less familiar with Conda so please report issues! (special thanks to @blue_hacker for this fix!)

```
$ conda create -n mlsecurity --copy python=3.6
$ source activate mlsecurity
$ pip install -r requirements.txt
```

### Repository structure

This repository contains some external submodules with modifications -- this is primarily due to making things compatible with Py3 or Jupyter (so we can view the results in JupyterHub). Some repositories you might want to take a look at installing individually or tracking changes are:

- [Cleverhans](https://github.com/tensorflow/cleverhans)
- [Foolbox](https://github.com/bethgelab/foolbox)
- [ModelExtraction](https://github.com/ftramer/Steal-ML)
- [EvadeML-Zoo](https://github.com/mzweilin/EvadeML-Zoo)

### Corrections?

If you find any issues in these code examples, feel free to submit an Issue or Pull Request. I appreciate your input!

### Questions?

Reach out to @kjam on Twitter or GitHub. @kjam is also often on freenode. :)

def aggregate_column(series):
    if is_categorical(series):
        return [','.join(set(series))]
    return [series.mean()]

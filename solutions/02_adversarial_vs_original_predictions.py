adv_x = np.expand_dims(adversarial.copy()[:, :, ::-1], axis=0)
adv_x = preprocess_input(adv_x)

img_x = np.expand_dims(example_image.copy(), axis=0)
img_x = preprocess_input(img_x)

adv_preds = kmodel.predict(adv_x)
print('Adversarial predictions:')
for pred in decode_predictions(adv_preds, top=3)[0]:
    print(pred)

orig_preds = kmodel.predict(img_x)
print('Original predictions:')
for pred in decode_predictions(orig_preds, top=3)[0]:
    print(pred)

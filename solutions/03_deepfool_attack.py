## Deepfool attack
example_image, label = foolbox.utils.imagenet_example()

dfmodel = foolbox.attacks.DeepFoolAttack(fmodel)
adversarial = dfmodel(example_image[:, :, ::-1], label)

## See predictions
adv_img = np.expand_dims(adversarial.copy(), axis=0)

adv_preds = kmodel.predict(adv_img)
print('Adversarial predictions:')
for pred in decode_predictions(adv_preds, top=3)[0]:
    print(pred)

## Plot images
plt.figure()

plt.subplot(1, 2, 1)
plt.title('Original')
plt.imshow(example_image / 255)
# division by 255 to convert [0, 255] to [0, 1]
plt.axis('off')

plt.subplot(1, 2, 2)
plt.title('Adversarial')
plt.imshow(adversarial[:, :, ::-1] / 255)
# division by 255 to convert [0, 255] to [0, 1]
plt.axis('off')


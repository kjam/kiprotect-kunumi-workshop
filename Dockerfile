FROM jupyter/minimal-notebook:83ed2c63671f
#FROM jupyter/tensorflow-notebook:83ed2c63671f

USER root
RUN apt-get update
RUN apt-get install -y curl wget git

#Set the working directory
USER jovyan
WORKDIR /home/jovyan/

# Modules
COPY requirements.txt /home/jovyan/requirements.txt
RUN conda install --yes pip tensorflow==1.8.0
RUN pip install -r /home/jovyan/requirements.txt

# Add files
COPY notebooks /home/jovyan/notebooks
COPY data /home/jovyan/data
COPY solutions /home/jovyan/solutions
COPY steal_ml /home/jovyan/steal_ml
RUN mkdir /home/jovyan/.keras
RUN mkdir /home/jovyan/.keras/models
RUN mkdir /home/jovyan/.keras/datasets

# Download weights & datasets
RUN wget -q https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels.h5  
RUN mv resnet50_weights_tf_dim_ordering_tf_kernels.h5 /home/jovyan/.keras/models/
RUN wget -q https://s3.amazonaws.com/deep-learning-models/image-models/imagenet_class_index.json
RUN mv imagenet_class_index.json /home/jovyan/.keras/models/
RUN wget -q https://s3.amazonaws.com/img-datasets/mnist.npz 
RUN mv mnist.npz /home/jovyan/.keras/datasets
RUN wget -q https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz 
RUN tar -xvzf cifar-10-python.tar.gz && mv cifar-10-* /home/jovyan/.keras/datasets
RUN cp /home/jovyan/.keras/datasets/cifar-10-python.tar.gz /home/jovyan/.keras/datasets/cifar-10-batches-py.tar.gz

# clone evademlzoo and install deps
RUN git clone https://github.com/kjam/EvadeML-Zoo.git 
RUN cd EvadeML-Zoo && git pull
RUN cd EvadeML-Zoo && git submodule init && git submodule update
RUN cd EvadeML-Zoo && mkdir downloads; curl -sL https://github.com/mzweilin/EvadeML-Zoo/releases/download/v0.1/downloads.tar.gz | tar xzv -C downloads


# Allow user to write to directory
USER root
RUN chown -R $NB_USER /home/jovyan \
    && chmod -R 774 /home/jovyan
USER $NB_USER

# Expose the notebook port
EXPOSE 8888

# Start the notebook server
CMD jupyter notebook --no-browser --port 8888 --ip=0.0.0.0 --NotebookApp.token='' --NotebookApp.disable_check_xsrf=True --NotebookApp.iopub_data_rate_limit=1.0e10

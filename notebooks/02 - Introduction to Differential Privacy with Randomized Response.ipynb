{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Introduction to Differential Privacy with Randomized Response\n",
    "\n",
    "In this notebook, we'll take a *very simple* view of differential privacy to try and implement a basic boolean (yes, no) randomized response. We will then investigate the measurement of epsilon for our response and evaluate the aggregate statistics of our implementation on several distributions.\n",
    "\n",
    "For further investigation into differential privacy guarantees and implmentations, please see the in-depth notebook included in this repository."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining Differential Privacy\n",
    "\n",
    "[Differential privacy](https://www.microsoft.com/en-us/research/publication/differential-privacy/) is a popular mechanism to quantitatively assess the privacy loss of a given probabilistic query schema or data transformation method. The fundamental equation of differntial privacy is given as\n",
    "\n",
    "$$ \n",
    "\\begin{equation}\n",
    "    \\mathrm{Pr}[\\mathcal{K}(D_1) \\in S] \\le exp(\\epsilon) \\times \\mathrm{Pr}[\\mathcal{K}(D_2) \\in S] \\label{eq:dp}\n",
    "\\end{equation}\n",
    "$$\n",
    "\n",
    "Here, $\\mathrm{Pr}[\\mathcal{K}(D_1) \\in S]$ is the probability of a\n",
    "randomized function $\\mathcal{K}$ to yield one of values in the $S$ when evaluating\n",
    "it on a given dataset $D_1$. The right side is identical to the left except\n",
    "that the function is now evaluated on a dataset $D_2$ that differs from $D_1$\n",
    "in at most one element. And finally, $\\epsilon$ is a parameter that describes\n",
    "how much information is leaked (or generated) by the function.\n",
    "\n",
    "But how might this work in practice? We can think of one simple implementation, which is randomized response. With randomized response we are able to return the true value with some probability. We can think of this like a coin-flip. Let's say we want to ask something where only one response might be seen as sensitive, such as: Have you ever stolen something? \n",
    "\n",
    "We might say to the respondents, flip a coin. If the flip turns up heads, say yes no matter what. Otherwise, say the true answer. This gives each individual respondent some guarantee of individual privacy (and plausable deniability).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from random import random, choice\n",
    "from matplotlib import pylab as pl\n",
    "\n",
    "def simple_randomized_response(prob, value):\n",
    "    \"\"\"With probability `prob` return the true value, \n",
    "       val -- otherwise return 1.\n",
    "    \"\"\"\n",
    "    if random() <= prob:\n",
    "        return value\n",
    "    return 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "simulated, real = [], []\n",
    "\n",
    "while len(simulated) < 1000:\n",
    "    real_val = choice([0, 1, 0, 0])\n",
    "    real.append(real_val)\n",
    "    simulated.append(simple_randomized_response(.5, real_val))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Your Turn: How many from our simulated runs answered truthfully?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load ../solutions/differential-privacy/view_runs.py\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How might we estimate the true distribution when given only the randomized response?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prob = .5\n",
    "\n",
    "estimate = []\n",
    "for sim in simulated:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load ../solutions/differential-privacy/estimate-val.py\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "pd.DataFrame({'real': real, 'simulated': simulated, \n",
    "              'estimated': estimate}).hist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Your Turn: How would this change if we had a different real distribution?\n",
    "\n",
    "- Can you run an experiment by modifying the outcome above?\n",
    "- How might your estimate change if you have knowledge of the population distribution?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Calculating $\\epsilon$\n",
    "\n",
    "In our differentially private scheme, the probability of adding the true attribute value to the database is $1-p$. The probability of adding a random value is therefore $p$ and the probability of that value being $0$ is $k$. So how can we relate this to our initial definition of differential privacy? Well, we can set $D_1$ and $D_2$ as the versions of our database **before** and **after** adding the person's data to it. Let's say that before adding the person's data there are $n$ $1$'s in the database. We can then use a query $\\mathcal{K}$ that returns the number of 1's in the database and choose our result set as $S = \\{n\\}$. Before adding the person to the database, $\\mathcal{K}(D_1)=n$ with certainty, hence $\\mathrm{Pr}(\\mathcal{K}(D_1))=1$. After adding the person's data, the probability that the query result is still $n$ can be calculated as follows, depending on the person's attribute value:\n",
    "\n",
    "* If a persons's attribute value is $0$, the probability that $\\mathcal{K}$ is unchanged after adding the data to the database is given as $1-p+p\\cdot k$.\n",
    "* If a person's attribute value is $1$, the probability that $\\mathcal{K}$ is unchanged after adding the data to the database is given as $p\\cdot k$.\n",
    "\n",
    "We therefore have the two equations\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray}\n",
    "\\mathrm{Pr}[\\mathcal{K}(D_1) \\in S | x_i=1] & = & 1 \\le \\exp{\\epsilon}\\cdot \\mathrm{Pr}[\\mathcal{K}(D_2) \\in S | x_i=1] = \\exp{\\epsilon}\\cdot p \\cdot k \\\\\n",
    "\\mathrm{Pr}[\\mathcal{K}(D_1) \\in S | x_i=0] & = & 1 \\le \\exp{\\epsilon}\\cdot \\mathrm{Pr}[\\mathcal{K}(D_2) \\in S | x_i=0] = \\exp{\\epsilon}\\cdot (1-p+p \\cdot k) \\\\\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "This yields\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray}\n",
    " \\epsilon & \\ge & -\\ln{\\left(p \\cdot k\\right)} \\\\\n",
    " \\epsilon & \\ge & -\\ln{\\left(1-p+p\\cdot k\\right)} \\\\\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "Since we're interested in an upper bound for $\\epsilon$ and since $-\\ln{\\left(1-p+p\\cdot k\\right)} \\le -\\ln{p\\cdot k}$, we obtain\n",
    "\n",
    "$$\n",
    "\\begin{equation}\n",
    "\\epsilon = -\\ln{\\left(p\\cdot k\\right)}\n",
    "\\end{equation}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load \"../solutions/differential-privacy/epsilon.py\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# so in our case...\n",
    "\n",
    "epsilon(.5, 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.exp(epsilon(.5, 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Protecting both responses\n",
    "\n",
    "- What about if either response was sensitive, and we needed to protect both yes and no?\n",
    "- How about if our response was not a simple yes or no; but instead multidimensional data?\n",
    "- What if we want to query a person over a longer period of time?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

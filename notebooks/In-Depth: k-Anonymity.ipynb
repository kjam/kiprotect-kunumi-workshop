{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 02. k-Anonymity\n",
    "\n",
    "In this notebook, we'll explore data anonymization, in particular \"k-anonymity\" [1]. We will implement a simple algorithm [2] to produce a k-anonymous dataset.\n",
    "\n",
    "For more reading on the topic, please see: \n",
    "\n",
    "- [k-Anonymity: A Model For Protecting Privacy. Latanya Sweeney](https://epic.org/privacy/reidentification/Sweeney_Article.pdf)\n",
    "- [Mondrian - Multidimensional k-Anonymity](https://www.utdallas.edu/~muratk/courses/privacy08f_files/MultiDim.pdf)\n",
    "\n",
    "## Introduction\n",
    "\n",
    "\n",
    "k-anonymity protects the privacy of individual persons by pooling their attributes into groups of at least $k$ people. The method is based on the assumption that we have a dataset that contains $N$ entries. Each entry consists of a list of $D$ attributes $X_i$ ($i \\in [0,D]$) that contain (non-sensitive) information about a person, such as age, gender, zip code of residence, etc. These attributes are called \"quasi-identifiers\", as combining several of them into a \"super-identifier\" can often uniquely identify a person even in large datasets (e.g. the combination of gender, age and zip code might be so specific that only a single person in a dataset has a given combination). In addition, the model assumes that the dataset contains a single sensitive attribute that contains e.g. information about a person's income and that we want to protect. The method can also be generalized to datasets with more than one sensitive attribute or datasets where there's no clear distinction between quasi-identifiers and sensitive attributes. For this case study we will look at the simple case though.\n",
    "\n",
    "Now, k-anonymity demands that we group individual rows/persons of our dataset into group of at least $k$ rows/persons and replace the quasi-identifier attributes of these rows with aggregate quantities, such that it is no longer possible to read the individual values. This protects people by ensuring that an adversary who knows all values of a person's quasi-identifier attributes can only find out which group a person might belong to but not know if the person is really in the dataset.\n",
    "\n",
    "A large problem of this approach is that it might happen that all people in a k-anonymous group possess the same value of the sensitive attribute. An adversary who knows that a person is in that k-anonymous group can then still learn the value of the sensitive attribute of that person with absolute certainty. This problem can be fixed by using an extension of k-anonymity called \"l-diversity\": l-diversity ensures that each k-anonymous group contains at least l different values of the sensitive attribute. Therefore, evne if an adversary can identify the group of a person he/she still would not be able to find out the value of that person's sensitive attribute with certainty. \n",
    "\n",
    "However, even when using l-diversity an adversary could still learn some information about a person's sensitive attribute using probabilistic reasoning: If, for example, 4 out of 5 people in a 5-anonymous group possess a given value of the sensitive attribute, an attacker can reason that a given person who he/she knows is part of the group will -with high probability- possess that value. Again, this problem can be fixed by extending k-anonymity using a so-called \"t-closeness\" criterion: t-closeness demands that the statistical distribution of the sensitive attribute values in each k-anonymous group is \"close\" to the overall distribution of that attribute in the entire dataset. Typically, the closeness can be measured using e.g. the Kullback-Leibler (KL) divergence. An adversary could then only learn a limited amount of information from comparing the distribution of the values in the group to the distribution in the entire dataset.\n",
    "\n",
    "Of course, k-anonymity, l-diversity and t-closeness all limit the amount of information that a legitimate user can learn from the data as well, so typically we need to balance the degree of privacy against the utility of the resulting data.\n",
    "\n",
    "## Implementing k-anonymity\n",
    "\n",
    "Turning a dataset into a k-anonymous (and possibly l-diverse or t-close) dataset is a complex problem, and finding the optimal partition into k-anonymous groups is an NP-hard problem. Fortunately, several practical algorithms exists that often produce \"good enough\" results by employing greedy search techniques.\n",
    "\n",
    "In this tutorial we will explore the so-called \"Mondrian\" algorithm (see link above), which uses a greedy search algorithm to parition the original data into smaller and smaller groups (if we plot the resulting partition boundaries in 2D they resemble the pictures by Piet Mondrian, hence the name).\n",
    "\n",
    "The algorithm assumes that we have converted all attributes into numerical or categorical values and that we're able to measure the \"span\" of a given data attribute $X_i$ (we'll explain that in more detail later).\n",
    "\n",
    "### Partitioning\n",
    "\n",
    "The algorithm proceeds then as follows to partition the data into k-anonymous gorups:\n",
    "\n",
    "1. Initialize the finished set of partitions to an empty set $P_{finished} = \\{\\}$.\n",
    "2. Initialize the working set of paritions to a set containing a partition with the entire dataset $P_{working} = \\{\\{1, 2,\\dots ,N\\}\\}$.\n",
    "4. While there are partitions in the working set, pop one partition from it and\n",
    "  * Calculate the relative spans of all columns in the partition.\n",
    "  * Sort the resulting columns by their span (in descending order) and iterate over them. For each column,\n",
    "      * Try to split the partition along that column using the median of the column values as the split point.\n",
    "      * Check if the resulting partitions are valid according to our k-anonymity (and possibly additional) criteria.\n",
    "      * If yes, add the two new partitions to the working set and break out of the loop.\n",
    "  * If no column produced a valid split, add the original partition to the set of finished partitions.\n",
    "5. Return the finished set of partitions\n",
    "\n",
    "### Data Aggregation\n",
    "\n",
    "After obtaining the partitions we still need to aggregate the values of the quasi identifiers and the sensitive attributes in each k-anonymous group. For this, we can e.g. replace numerical attributes with their range (e.g. \"age: 24-28\") and categorical attributes with their union (e.g. \"employment-group: [self-employed, employee, worker]\"), though other aggregations are possible. Methods like [Anatomy](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.156.9150&rep=rep1&type=pdf) even preserve the micro-data in each group, which increases re-identification risk though.\n",
    "\n",
    "## Usage of the Anonymized Data\n",
    "\n",
    "We can then use the anonymized data for privacy-preserving machine learning, e.g. using scikit-learn. If we have time at the end of this part we'll cover this as well. With the dataset generation method discussed here you can use the resulting datasets just as you would use a normal dataset though, so there's really nothing special about the anonymized data here.\n",
    "\n",
    "Let's get started with the anonymization!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we use Pandas to work with the data as it makes working with categorical data very easy\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this is a list of the column names in our dataset (as the file doesn't contain any headers)\n",
    "names = (\n",
    "    'age',\n",
    "    'workclass', #Private, Self-emp-not-inc, Self-emp-inc, Federal-gov, Local-gov, State-gov, Without-pay, Never-worked.\n",
    "    'fnlwgt', # \"weight\" of that person in the dataset (i.e. how many people does that person represent) -> https://www.kansascityfed.org/research/datamuseum/cps/coreinfo/keyconcepts/weights\n",
    "    'education',\n",
    "    'education-num',\n",
    "    'marital-status',\n",
    "    'occupation',\n",
    "    'relationship',\n",
    "    'race',\n",
    "    'sex',\n",
    "    'capital-gain',\n",
    "    'capital-loss',\n",
    "    'hours-per-week',\n",
    "    'native-country',\n",
    "    'income',\n",
    ")\n",
    "\n",
    "# some fields are categorical and will require special treatment\n",
    "categorical = set((\n",
    "    'workclass',\n",
    "    'education',\n",
    "    'marital-status',\n",
    "    'occupation',\n",
    "    'relationship',\n",
    "    'sex',\n",
    "    'native-country',\n",
    "    'race',\n",
    "    'income',\n",
    "))\n",
    "df = pd.read_csv(\"../data/k-anonymity/adult.all.txt\", sep=\", \", header=None, names=names, index_col=False, engine='python');# We load the data using Pandas"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for name in categorical:\n",
    "    df[name] = df[name].astype('category')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "**Implement a function that returns the spans (max-min for numerical columns, number of different values for categorical columns) of all columns for a partition of a dataframe.** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# % load \"../solutions/k-anonymity/spans.py\"\n",
    "\n",
    "def get_spans(df, partition, scale=None):\n",
    "    \"\"\"\n",
    "    :param        df: the dataframe for which to calculate the spans\n",
    "    :param partition: the partition for which to calculate the spans\n",
    "    :param     scale: if given, the spans of each column will be divided\n",
    "                      by the value in `scale` for that column\n",
    "    :        returns: The spans of all columns in the partition\n",
    "    \"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_spans = get_spans(df, df.index)\n",
    "full_spans"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "**Implement a `split` function that takes a dataframe, partition and column and returns two partitions that split the given partition such that all rows with values of the column `column` below the median are in one partition and all rows with values above or equal to the median are in the other.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load \"../solutions/k-anonymity/split.py\"\n",
    "\n",
    "def split(df, partition, column):\n",
    "    \"\"\"\n",
    "    :param        df: The dataframe to split\n",
    "    :param partition: The partition to split\n",
    "    :param    column: The column along which to split\n",
    "    :        returns: A tuple containing a split of the original partition\n",
    "    \"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "Now that we have all helper functions in place, we can implement the partition algorithm discussed above:\n",
    "\n",
    "**Implement the partitioning algorithm discussed above, using a k-anonymous criterion for the partitions you create.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load '../solutions/k-anonymity/partition.py'\n",
    "\n",
    "def is_k_anonymous(df, partition, sensitive_column, k=3):\n",
    "    \"\"\"\n",
    "    :param               df: The dataframe on which to check the partition.\n",
    "    :param        partition: The partition of the dataframe to check.\n",
    "    :param sensitive_column: The name of the sensitive column\n",
    "    :param                k: The desired k\n",
    "    :returns               : True if the partition is valid according to our k-anonymity criteria, False otherwise.\n",
    "    \"\"\"\n",
    "\n",
    "def partition_dataset(df, feature_columns, sensitive_column, scale, is_valid):\n",
    "    \"\"\"\n",
    "    :param               df: The dataframe to be partitioned.\n",
    "    :param  feature_columns: A list of column names along which to partition the dataset.\n",
    "    :param sensitive_column: The name of the sensitive column (to be passed on to the `is_valid` function)\n",
    "    :param            scale: The column spans as generated before.\n",
    "    :param         is_valid: A function that takes a dataframe and a partition and returns True if the partition is valid.\n",
    "    :returns               : A list of valid partitions that cover the entire dataframe.\n",
    "    \"\"\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's try this on our dataset! To keep things simple, we will at first select only two columns from the dataset that we apply the partitioning to. This makes it easier to check/visualize the result and speed up the execution (the naive algorithm can take several minutes when running on the entire dataset) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we apply our partitioning method to two columns of our dataset, using \"income\" as the sensitive attribute\n",
    "feature_columns = ['age', 'education-num']\n",
    "sensitive_column = 'income'\n",
    "finished_partitions = partition_dataset(df, feature_columns, sensitive_column, full_spans, is_k_anonymous)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we get the number of partitions that were created\n",
    "len(finished_partitions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's visualize the created partitions! To do that, we will write functions to get the rectangular bounds of a partition along two columns. We can then plot these rects to see how our partitioning function divides the dataset. If we perform the partition only along the two columns selected for plotting then the resulting rects should not overlap and cover the entire dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pylab as pl\n",
    "import matplotlib.patches as patches"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def build_indexes(df):\n",
    "    indexes = {}\n",
    "    for column in categorical:\n",
    "        values = sorted(df[column].unique())\n",
    "        indexes[column] = { x : y for x, y in zip(values, range(len(values)))}\n",
    "    return indexes\n",
    "\n",
    "def get_coords(df, column, partition, indexes, offset=0.1):\n",
    "    if column in categorical:\n",
    "        sv = df[column][partition].sort_values()\n",
    "        l, r = indexes[column][sv[sv.index[0]]], indexes[column][sv[sv.index[-1]]]+1.0\n",
    "    else:\n",
    "        sv = df[column][partition].sort_values()\n",
    "        next_value = sv[sv.index[-1]]\n",
    "        larger_values = df[df[column] > next_value][column]\n",
    "        if len(larger_values) > 0:\n",
    "            next_value = larger_values.min()\n",
    "        l = sv[sv.index[0]]\n",
    "        r = next_value\n",
    "    # we add some offset to make the partitions more easily visible\n",
    "    l -= offset\n",
    "    r += offset\n",
    "    return l, r\n",
    "\n",
    "def get_partition_rects(df, partitions, column_x, column_y, indexes, offsets=[0.1, 0.1]):\n",
    "    rects = []\n",
    "    for partition in partitions:\n",
    "        xl, xr = get_coords(df, column_x, partition, indexes, offset=offsets[0])\n",
    "        yl, yr = get_coords(df, column_y, partition, indexes, offset=offsets[1])\n",
    "        rects.append(((xl, yl),(xr, yr)))\n",
    "    return rects\n",
    "\n",
    "def get_bounds(df, column, indexes, offset=1.0):\n",
    "    if column in categorical:\n",
    "        return 0-offset, len(indexes[column])+offset\n",
    "    return df[column].min()-offset, df[column].max()+offset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we calculate the bounding rects of all partitions that we created\n",
    "indexes = build_indexes(df)\n",
    "column_x, column_y = feature_columns[:2]\n",
    "rects = get_partition_rects(df, finished_partitions, column_x, column_y, indexes, offsets=[0.0, 0.0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# let's see how our rects look like\n",
    "rects[:10]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we plot the rects\n",
    "def plot_rects(df, ax, rects, column_x, column_y, edgecolor='black', facecolor='none'):\n",
    "    for (xl, yl),(xr, yr) in rects:\n",
    "        ax.add_patch(patches.Rectangle((xl,yl),xr-xl,yr-yl,linewidth=1,edgecolor=edgecolor,facecolor=facecolor, alpha=0.5))\n",
    "    ax.set_xlim(*get_bounds(df, column_x, indexes))\n",
    "    ax.set_ylim(*get_bounds(df, column_y, indexes))\n",
    "    ax.set_xlabel(column_x)\n",
    "    ax.set_ylabel(column_y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pl.figure(figsize=(20,20))\n",
    "ax = pl.subplot(111)\n",
    "plot_rects(df, ax, rects, column_x, column_y, facecolor='r')\n",
    "# we can also plot the datapoints themselves\n",
    "pl.scatter(df[column_x], df[column_y])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generating an k-Anonymous Dataset\n",
    "\n",
    "Of course, to use the data we want to produce a new dataset that contains one row for each partition and value of the sensitive attribute. To do this, we need to aggregate the columns in each partition.  Let's do this!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def agg_categorical_column(series):\n",
    "    return [','.join(set(series))]\n",
    "\n",
    "def agg_numerical_column(series):\n",
    "    return [series.mean()]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def build_anonymized_dataset(df, partitions, feature_columns, sensitive_column, max_partitions=None):\n",
    "    aggregations = {}\n",
    "    for column in feature_columns:\n",
    "        if column in categorical:\n",
    "            aggregations[column] = agg_categorical_column\n",
    "        else:\n",
    "            aggregations[column] = agg_numerical_column\n",
    "    rows = []\n",
    "    for i, partition in enumerate(partitions):\n",
    "        if i % 100 == 1:\n",
    "            print(\"Finished {} partitions...\".format(i))\n",
    "        if max_partitions is not None and i > max_partitions:\n",
    "            break\n",
    "        grouped_columns = df.loc[partition].agg(aggregations, squeeze=False)\n",
    "        sensitive_counts = df.loc[partition].groupby(sensitive_column).agg({sensitive_column : 'count'})\n",
    "        values = grouped_columns.iloc[0].to_dict()\n",
    "        for sensitive_value, count in sensitive_counts[sensitive_column].items():\n",
    "            if count == 0:\n",
    "                continue\n",
    "            values.update({\n",
    "                sensitive_column : sensitive_value,\n",
    "                'count' : count,\n",
    "\n",
    "            })\n",
    "            rows.append(values.copy())\n",
    "    return pd.DataFrame(rows)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dfn = build_anonymized_dataset(df, finished_partitions, feature_columns, sensitive_column)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we sort the resulting dataframe using the feature columns and the sensitive attribute\n",
    "dfn.sort_values(feature_columns+[sensitive_column])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Implementing l-diversity (the naive way)\n",
    "\n",
    "Now let's see how we can implement l-diversity in order to protect the privacy of the persons in the dataset even better. To implement l-diversity, we can do the following things:\n",
    "\n",
    "* Modify our `is_valid` function to not only check for the size of a given partition but also ensure that the values of the sensitive attribute in the partition are diverse enough.\n",
    "* Modify the `split` function to produce splits that are diverse (if possible)\n",
    "\n",
    "Here we will only implement the first point to keep things simple, please keep in mind that this is not the smartest way to implement l-diversity, as our \"naive\" splitting function might produce invalid splits even when it would actually be possible to produce a valid one."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "**Implement a validator function that returns `True` if a given partition contains at least `l` different values of the sensitive attribute, `False` otherwise.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load \"../solutions/k-anonymity/diversity.py\"\n",
    "\n",
    "def is_l_diverse(df, partition, sensitive_column, l=2):\n",
    "    \"\"\"\n",
    "    :param               df: The dataframe for which to check l-diversity\n",
    "    :param        partition: The partition of the dataframe on which to check l-diversity\n",
    "    :param sensitive_column: The name of the sensitive column\n",
    "    :param                l: The minimum required diversity of sensitive attribute values in the partition\n",
    "    \"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# now let's apply this method to our data and see how the result changes\n",
    "finished_l_diverse_partitions = partition_dataset(df, feature_columns, sensitive_column, full_spans, lambda *args: is_k_anonymous(*args) and is_l_diverse(*args))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(finished_l_diverse_partitions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "column_x, column_y = feature_columns[:2]\n",
    "l_diverse_rects = get_partition_rects(df, finished_l_diverse_partitions, column_x, column_y, indexes, offsets=[0.0, 0.0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pl.figure(figsize=(20,20))\n",
    "ax = pl.subplot(111)\n",
    "plot_rects(df, ax, l_diverse_rects, column_x, column_y, edgecolor='b', facecolor='b')\n",
    "plot_rects(df, ax, rects, column_x, column_y, facecolor='r')\n",
    "pl.scatter(df[column_x], df[column_y])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# again we build an anonymized dataset from the l-diverse partitions\n",
    "dfl = build_anonymized_dataset(df, finished_l_diverse_partitions, feature_columns, sensitive_column)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's see how l-diversity improves the anonymity of our dataset\n",
    "dfl.sort_values([column_x, column_y, sensitive_column])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Implementing t-closeness\n",
    "\n",
    "As we can see, for regions where the value diversity is low, our l-diverse method produces partitions that contain a very large number of entries for one value of the sensitive attribute and only one entry for the other value. This is not ideal as while there is \"plausible deniability\" for a person in the dataset (after all the person could be the one \"outlier\") but an adversary can still be very certain about the person's attribute value in that case.\n",
    "\n",
    "t-closeness solves this problem by making sure that the distribution of sensitive attribute values in a given partition is similar to the distribution of the values in the overall dataset.\n",
    "\n",
    "We'll implement a naive (and not efficient / correct) version of t-closeness below. As with the l-diversity case, it would be better to tailor the `split` function to produce partitions that are t-close, which would increase the efficiency of the method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "**Implement a version of the `is_valid` function that returns `True` if the partition is diverse enough and `False` otherwise. To measure diversity, calculate the Kolmogorov-Smirnov distance between the empirical\n",
    "probability distribution of the sensitive attribute over the entire dataset vs. the distribution over\n",
    "the partition. Hint: the Kolmogorov-Smirnov distance is the maximum distance between the two distributions.\n",
    "You can assume that the sensitive attribute is a categorical value.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# here we generate the global frequencies for the sensitive column \n",
    "global_freqs = {}\n",
    "total_count = float(len(df))\n",
    "group_counts = df.groupby(sensitive_column)[sensitive_column].agg('count')\n",
    "for value, count in group_counts.to_dict().items():\n",
    "    p = count/total_count\n",
    "    global_freqs[value] = p"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "global_freqs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load \"../solutions/k-anonymity/closeness.py\"\n",
    "\n",
    "def is_t_close(df, partition, sensitive_column, global_freqs, p=0.2):\n",
    "    \"\"\"\n",
    "    :param               df: The dataframe for which to check l-diversity\n",
    "    :param        partition: The partition of the dataframe on which to check l-diversity\n",
    "    :param sensitive_column: The name of the sensitive column\n",
    "    :param     global_freqs: The global frequencies of the sensitive attribute values\n",
    "    :param                p: The maximum allowed Kolmogorov-Smirnov distance\n",
    "    \"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's apply this to our dataset\n",
    "finished_t_close_partitions = partition_dataset(df, feature_columns, sensitive_column, full_spans, lambda *args: is_k_anonymous(*args) and is_t_close(*args, global_freqs))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(finished_t_close_partitions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dft = build_anonymized_dataset(df, finished_t_close_partitions, feature_columns, sensitive_column)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's see how t-closeness fares\n",
    "dft.sort_values([column_x, column_y, sensitive_column])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "column_x, column_y = feature_columns[:2]\n",
    "t_close_rects = get_partition_rects(df, finished_t_close_partitions, column_x, column_y, indexes, offsets=[0.0, 0.0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pl.figure(figsize=(20,20))\n",
    "ax = pl.subplot(111)\n",
    "plot_rects(df, ax, t_close_rects, column_x, column_y, edgecolor='b', facecolor='b')\n",
    "#plot_rects(df, ax, rects, column_x, column_y, facecolor='r')\n",
    "pl.scatter(df[column_x], df[column_y])"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
